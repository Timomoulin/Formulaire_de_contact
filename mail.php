<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;


require 'PHPMailer-master/src/Exception.php';
require 'PHPMailer-master/src/PHPMailer.php';
require 'PHPMailer-master/src/SMTP.php';


$name = secure_input($_POST['name']);
$message = secure_input($_POST['message']);

$email = secure_input($_POST["email"]);
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
  $emailErr = "Invalid email format";
}

function secure_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

 $mail = new PHPmailer();
 $mail->IsSMTP();
 //$mail->SMTPDebug = 2;
 //$mail->SMTPDebug = SMTP::DEBUG_SERVER;
 $mail->Host='smtp.gmail.com';
 $mail->Port = 465;
 $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
 $mail->SMTPOptions = array(
  'ssl' => array(
      'verify_peer' => false,
      'verify_peer_name' => false,
      'allow_self_signed' => true
      )
  );
//  $mail->SMTPSecure = 'tls';
//  $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
 $mail->SMTPAuth = true;
 $mail->SMTPSecure = 'ssl';
 $mail->Username=""; //votre gmail
 $mail->Password=""; // mdp gmail

 $mail->setFrom('', 'Formulaire de contact'); // votre gmail
 $mail->AddAddress($email);

 $mail->IsHTML(true);
 $mail->Subject='message de '.$name.' email '.$email;
 $mail->Body=$message;
 $mail->CharSet="UTF-8";
 if(!$mail->Send()){ //Teste le return code de la fonction
   echo $mail->ErrorInfo; //Affiche le message d'erreur 
 }
 else{     
   echo 'Mail envoyé avec succès';
 }
 $mail->SmtpClose();
 unset($mail);

?> 


